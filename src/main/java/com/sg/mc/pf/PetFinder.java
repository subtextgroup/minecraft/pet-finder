package com.sg.mc.pf;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ryan on 4/9/2017.
 */
public class PetFinder extends JavaPlugin implements Listener, CommandExecutor {


    @Override
    public void onEnable() {

        getCommand("petfinder").setExecutor(this);

        getServer().getPluginManager().registerEvents(this, this);
        super.onEnable();
    }


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {

        if(!(commandSender instanceof Player)) {
            return false;
        }



        Player p = (Player)commandSender;

        List<LivingEntity> livingEntities = p.getWorld().getLivingEntities();

        if(livingEntities == null) {
            p.sendMessage("Everything is dead :(");
            return false;
        }

        String name = null;
        String typeArgs = null;
        boolean list = false;
        if(args.length > 0) {
            if(args[0].toLowerCase().startsWith("name:")) {
                name = Arrays.stream(args).collect(Collectors.joining(" ")).substring("name:".length());
            } else if(args[0].toLowerCase().startsWith("type:")) {
                typeArgs = Arrays.stream(args).collect(Collectors.joining(" ")).substring("type:".length());
            } else if(args[0].toLowerCase().equals("list")) {
                list = true;
            }
        }

        final Class<?> animalClass;
        if(typeArgs == null) {
            typeArgs = "";
        }
        switch(typeArgs.toLowerCase()) {
            case "wolf":
            case "dog":
                animalClass = Wolf.class;
                break;
            case "llama":
                animalClass = Llama.class;
                break;
            case "horse":
                animalClass = Horse.class;
                break;
            case "cat":
            case "ocelot":
                animalClass = Ocelot.class;
                break;
            default:
                animalClass = null;
        }
        List<LivingEntity> entities;


        if(name != null) {
            final String animalName = name;
            entities = livingEntities.stream()
                    .filter(le -> animalName.equalsIgnoreCase(le.getCustomName()))
                    .collect(Collectors.toList());
        } else {
            entities = livingEntities.stream()
                    .filter(Tameable.class::isInstance)
                    .map(Tameable.class::cast)
                    .filter(Tameable::isTamed)
                    .filter(t -> p.equals(t.getOwner()) && (animalClass == null || animalClass.isInstance(t)))
                    .map(LivingEntity.class::cast)
                    .collect(Collectors.toList());

        }
        if (entities.size() == 0) {
            p.sendMessage("Could not find any matching animals :(");
            return true;
        } else {
            entities.forEach(pet -> pet.teleport(p));
            return true;
        }

    }

}
