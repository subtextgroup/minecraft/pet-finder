**Pet Finder**


Lost a tamed cat or dog after teleporting? Use /petfinder to teleport all your pets to you.

Use /petfinder name:\<pet name\> to teleport named entities to you.

Use /petfinder type:\<animal type\> to teleport tamed animals of the specified type to you.